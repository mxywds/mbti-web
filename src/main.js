import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

import './assets/main.css'

import 'primevue/resources/themes/bootstrap4-light-purple/theme.css'
import 'primevue/resources/primevue.min.css'
import 'primeicons/primeicons.css'

import PrimeVue from 'primevue/config';

const app = createApp(App)
app.use(PrimeVue, {ripple: true});
app.use(router)

app.mount('#app')
